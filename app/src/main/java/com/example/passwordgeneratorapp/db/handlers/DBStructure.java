package com.example.passwordgeneratorapp.db.handlers;


/**
 * Holds all Database related keywords and statements, like Schema/Table/Column names
 * and Create table Statements.
 */
public interface DBStructure {

    int DB_VERSION = 1;
    String DB_NAME = "password_generator";
    String COLUMN_ID = "_id";

    String TABLE_USER = "users";
    String COLUMN_USERNAME = "username";
    String COLUMN_MASTER_HASH = "master_hash";
    String COLUMN_SALT = "salt";

    String TABLE_SERVICE_MAPPING = "service_mapping";
    String COLUMN_USER_ID = "user_id";
    String COLUMN_SERVICE_NAME = "service_name";
    String COLUMN_SERVICE_USER_NAME = "service_user_name";
    String COLUMN_LATEST_VERSION = "latest_version";

    String SQL_CREATE_USERS = String.format(
            "CREATE TABLE %s (" +
                    "%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "%s TEXT NOT NULL," +
                    "%s TEXT NOT NULL," +
                    "%s TEXT NOT NULL" +
                    ");",
            TABLE_USER,
            COLUMN_ID,
            COLUMN_USERNAME,
            COLUMN_MASTER_HASH,
            COLUMN_SALT);

    String SQL_CREATE_SERVICE_MAPPING = String.format(
            "CREATE TABLE %s (" +
                    "%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "%s INTEGER NOT NULL," +
                    "%s TEXT NOT NULL," +
                    "%s TEXT NOT NULL," +
                    "%s INTEGER NOT NULL" +
                    ");",
            TABLE_SERVICE_MAPPING,
            COLUMN_ID,
            COLUMN_USER_ID,
            COLUMN_SERVICE_NAME,
            COLUMN_SERVICE_USER_NAME,
            COLUMN_LATEST_VERSION);
}
