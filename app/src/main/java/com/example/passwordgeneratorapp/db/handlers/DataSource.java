package com.example.passwordgeneratorapp.db.handlers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.passwordgeneratorapp.generator.PasswordGeneratorHelper;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.List;

/**
 * Handles all database transactions of this application.
 */
public class DataSource implements DBStructure {
    private static final String TAG = DataSource.class.getSimpleName();

    private SQLiteDatabase sqLiteDatabase;
    private DBHelper dbHelper;
    private PasswordGeneratorHelper pGHelper = new PasswordGeneratorHelper();

    /**
     * Makes sure the database is build.
     *
     * @param context Activity
     */
    public DataSource(Context context) {
        Log.d(TAG, "DataSource is creating DBHelper");
        dbHelper = new DBHelper(context);
    }

    /**
     * Allows interacting with database.
     */
    public void open() {
        sqLiteDatabase = dbHelper.getWritableDatabase();
    }

    /**
     * Closes possible database interactions.
     */
    public void close() {
        sqLiteDatabase.close();
    }

    /**
     * Queries users table for specific userId.
     *
     * @param username the name of the requested userId
     * @return userId
     */
    public int getUserId(String username) {
        String query = String.format(
                "SELECT %s FROM %s WHERE %s= '%s';",
                COLUMN_ID, TABLE_USER, COLUMN_USERNAME, username);
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        cursor.moveToFirst();
        int userId = cursor.getInt(0);
        cursor.close();
        return userId;
    }

    /**
     * Queries users table for specific username.
     *
     * @param username the requested username
     * @return true if if user exists.
     */
    private boolean userExists(String username) {
        String query = String.format(
                "SELECT count(*) FROM %s WHERE %s = '%s';",
                TABLE_USER, COLUMN_USERNAME, username);
        Log.d(TAG, "checkUserExists: " + query);
        return checkExists(query);
    }

    /**
     * Queries service_mapping table for specified criteria.
     *
     * @param userId          id of current user.
     * @param serviceName     name of service
     * @param serviceUserName name of service username
     * @return true if service already exists
     */
    private boolean serviceExists(int userId, String serviceName, String serviceUserName) {
        String query = String.format(
                "SELECT count(*) FROM %s WHERE %s = " + userId + " AND %s = '%s' AND %s = '%s';",
                TABLE_SERVICE_MAPPING,
                COLUMN_USER_ID,
                COLUMN_SERVICE_NAME, serviceName,
                COLUMN_SERVICE_USER_NAME, serviceUserName);
        Log.d(TAG, "checkService: " + query);
        return checkExists(query);
    }

    /**
     * If count of result of query is bigger than 0 then return true.
     *
     * @param query sql query
     * @return true if exists
     */
    private boolean checkExists(String query) {
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        cursor.close();
        return (count > 0);
    }


    /**
     * Take user inputs (username, password), generates a database hash and salt and inserts
     * them in users table.
     *
     * @param credentials List of user inputs.
     * @return true if new user was created successfully.
     */
    public boolean createNewUser(List<String> credentials) {
        List<String> masterHashMapping;
        ContentValues contentValues = new ContentValues();

        String username = credentials.get(0).toLowerCase();
        String password = credentials.get(1);

        if (!userExists(username)) {
            try {
                masterHashMapping = pGHelper.generatePasswordHashForDb(password);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return false;
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
                return false;
            }
            String masterHash = masterHashMapping.get(0);
            String masterSalt = masterHashMapping.get(1);

            contentValues.put(COLUMN_USERNAME, username);
            contentValues.put(COLUMN_MASTER_HASH, masterHash);
            contentValues.put(COLUMN_SALT, masterSalt);

            sqLiteDatabase.insert(TABLE_USER, null, contentValues);
            Log.d(TAG, "createNewUser: New User created");
            return true;
        }
        return false;
    }

    /**
     * Get a Cursor object of all services from service_mapping for specified userId.
     *
     * @param userId specified userId
     * @return Cursor object.
     */
    public Cursor getServices(int userId) {
        String query = String.format(
                "SELECT %s, %s, %s FROM %s WHERE %s = %s ;",
                COLUMN_SERVICE_NAME,
                COLUMN_SERVICE_USER_NAME,
                COLUMN_LATEST_VERSION,
                TABLE_SERVICE_MAPPING,
                COLUMN_USER_ID,
                userId
        );
        return sqLiteDatabase.rawQuery(query, null);
    }

    /**
     * Queries users table for requested hash and salt and passes them to
     * PasswordGeneratorHelper.validatePassword.
     *
     * @param credentials user inputs
     * @return true if password matches.
     */
    public boolean validateUser(List<String> credentials) {
        boolean passwordMatches = false;

        String credentials_username = credentials.get(0).toLowerCase();
        String credentials_password = credentials.get(1);

        String query = String.format("SELECT %s, %s FROM %S WHERE %s = '%s';",
                COLUMN_MASTER_HASH, COLUMN_SALT, TABLE_USER, COLUMN_USERNAME, credentials_username);
        Log.d(TAG, "validateUser: " + query);
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        boolean exists = cursor.getCount() > 0;
        if (!exists) {
            cursor.close();
            return false;
        }
        cursor.moveToFirst();
        List<String> queryResults = Arrays.asList(
                cursor.getString(0),
                cursor.getString(1));
        cursor.close();
        try {
            passwordMatches = pGHelper.validatePassword(credentials_password, queryResults);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return passwordMatches;
    }

    /**
     * Inserts a new service into service_mapping table.
     *
     * @param userId          id of requested user
     * @param serviceName     name of requested service
     * @param serviceUserName name of requested serviceUserName
     * @return true if stored successfully
     */
    public boolean saveNewServiceSuccessful(int userId, String serviceName, String serviceUserName) {

        if (!serviceExists(userId, serviceName, serviceUserName)) {
            Log.d(TAG, String.format(
                    "saveNewServiceSuccessful: new Entry" +
                            "\nUser ID: %s" +
                            "\nService Name: %s" +
                            "\nService User Name: %s" +
                            "\nLatest Version: %s",
                    userId, serviceName, serviceUserName, 1));
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_USER_ID, userId);
            contentValues.put(COLUMN_SERVICE_NAME, serviceName);
            contentValues.put(COLUMN_SERVICE_USER_NAME, serviceUserName);
            contentValues.put(COLUMN_LATEST_VERSION, 1);
            sqLiteDatabase.insert(TABLE_SERVICE_MAPPING, null, contentValues);
            return true;
        } else {
            Log.d(TAG, String.format("saveNewServiceSuccessful: " +
                            "Service %s with ServiceUsername %s already exists!",
                    serviceName, serviceUserName));
            return false;
        }

    }


    /**
     * Updates latestVersion field in service_mapping table by +1.
     *
     * @param userId          id of requested user
     * @param serviceName     name of requested service
     * @param serviceUserName name of requested serviceUserName
     */
    public void raiseVersion(int userId, String serviceName, String serviceUserName) {
        ContentValues contentValues = new ContentValues();
        String query = String.format(
                "SELECT %s FROM %s " +
                        "WHERE %s = %s AND %s = '%s' and %s = '%s';",
                COLUMN_LATEST_VERSION,
                TABLE_SERVICE_MAPPING,
                COLUMN_USER_ID, userId,
                COLUMN_SERVICE_NAME, serviceName,
                COLUMN_SERVICE_USER_NAME, serviceUserName
        );
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        cursor.moveToFirst();
        Log.d(TAG, "raiseVersion: " + cursor.getInt(0));
        int newVersion = cursor.getInt(0) + 1;
        cursor.close();
        contentValues.put(COLUMN_LATEST_VERSION, newVersion);

        sqLiteDatabase.update(
                TABLE_SERVICE_MAPPING,
                contentValues,
                COLUMN_USER_ID + " = ? AND " +
                        COLUMN_SERVICE_NAME + " = ? AND " +
                        COLUMN_SERVICE_USER_NAME + " = ?",
                new String[]{
                        String.valueOf(userId),
                        serviceName,
                        serviceUserName}
        );
    }

    /**
     * Deletes entry of requested serviceUserName.
     *
     * @param userId                 of requested user
     * @param currentService         name of requested service
     * @param currentServiceUserName name of requested serviceUserName
     */
    public void deleteServiceUserName(
            int userId,
            String currentService,
            String currentServiceUserName) {
        sqLiteDatabase.delete(
                TABLE_SERVICE_MAPPING,
                COLUMN_USER_ID + " = ? AND " +
                        COLUMN_SERVICE_NAME + " = ? AND " +
                        COLUMN_SERVICE_USER_NAME + " = ?",
                new String[]{
                        String.valueOf(userId),
                        currentService,
                        currentServiceUserName}
        );
    }
}
