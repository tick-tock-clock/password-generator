package com.example.passwordgeneratorapp.db.handlers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


/**
 * Initializes Database when needed by creating new DBHelper object.
 */
public class DBHelper extends SQLiteOpenHelper implements DBStructure {

    private static final String TAG = DBHelper.class.getSimpleName();

    DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        Log.d(TAG, "DBHelper created: " + getDatabaseName());
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            Log.d(TAG, String.format("creating: %s\n\n%s",
                    SQL_CREATE_USERS, SQL_CREATE_SERVICE_MAPPING));
            db.execSQL(SQL_CREATE_USERS);
            db.execSQL(SQL_CREATE_SERVICE_MAPPING);

        } catch (Exception ex) {
            Log.d(TAG, "onCreate: failed to create Tables.");
            Log.e(TAG, ex.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
