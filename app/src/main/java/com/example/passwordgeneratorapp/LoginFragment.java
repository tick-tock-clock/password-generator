package com.example.passwordgeneratorapp;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.passwordgeneratorapp.db.handlers.DataSource;
import com.example.passwordgeneratorapp.generator.PasswordGeneratorFragment;

import java.util.Arrays;
import java.util.List;

/** Ensures that a user exists in the database. */
public class LoginFragment extends Fragment {
    private static final String TAG = PasswordGeneratorFragment.class.getSimpleName();

    private DataSource dataSource;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    private EditText usernameInput;
    private EditText passwordInput;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.login_layout, container, false);
        dataSource = new DataSource(getActivity());
        fragmentManager = getFragmentManager();

        usernameInput = fragmentView.findViewById(R.id.id_login_username_input);
        passwordInput = fragmentView.findViewById(R.id.id_login_password_input);

        Button loginButton = fragmentView.findViewById(R.id.id_login_login_btn);
        Button createNewUserButton = fragmentView.findViewById(R.id.id_login_create_btn);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        createNewUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = fragmentManager.beginTransaction();
                CreateUserFragment createUserFragment = new CreateUserFragment();
                fragmentTransaction.replace(R.id.ui_container, createUserFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        return fragmentView;
    }

    /**
     * Takes the input and hands it over to DataSource.validateUser. If successful then redirect
     * to PasswordGeneratorFragment with username and userId passed as bundle.
     */
    private void login() {
        Log.d(TAG, "login: Trying to log in.");
        String username = usernameInput.getText().toString();
        String masterKey = passwordInput.getText().toString();

        List<String> credentials = Arrays.asList(username, masterKey);
        dataSource.open();
        if (dataSource.validateUser(credentials)) {
            Log.d(TAG, "login: Successfull");
            int userId = dataSource.getUserId(username);

            Toast.makeText(
                    getActivity(),
                    "Login successful.",
                    Toast.LENGTH_SHORT).show();

            Bundle args = new Bundle();
            args.putString("username", username);
            args.putInt("userId", userId);

            fragmentTransaction = fragmentManager.beginTransaction();
            PasswordGeneratorFragment pGFragment = new PasswordGeneratorFragment();
            pGFragment.setArguments(args);
            fragmentTransaction.replace(R.id.ui_container, pGFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        } else {
            Toast.makeText(
                    getActivity(),
                    "Incorrect Username or Master Key.",
                    Toast.LENGTH_SHORT).show();
        }
        dataSource.close();
    }

}
