package com.example.passwordgeneratorapp.generator;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.passwordgeneratorapp.LoginFragment;
import com.example.passwordgeneratorapp.R;
import com.example.passwordgeneratorapp.db.handlers.DataSource;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

/**
 * Handles the UI and event handling of generator_layout.xml
 */
public class PasswordGeneratorFragment
        extends Fragment {
    private static final String TAG = PasswordGeneratorFragment.class.getSimpleName();

    private DataSource dataSource;
    private FragmentManager fragmentManager;
    private PasswordGeneratorHelper pGHelper = new PasswordGeneratorHelper();

    private int userId;
    private String username;
    private LinkedHashMap<String, LinkedHashMap<String, Object>> userServices;

    private Spinner serviceNameSpinner;
    private Spinner serviceUserNameSpinner;
    private Spinner serviceVersionSpinner;
    private EditText passwordOutputEditText;
    private EditText passwordLengthEditText;
    private CheckBox specialCheckBox;

    private List<String> serviceNameArray;
    private List<String> serviceUserNameArray;

    private String currentService;
    private String currentServiceUserName;
    private int currentVersion;


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_delete_service).setVisible(true);
        menu.findItem(R.id.action_delete_service).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Log.d(TAG, "onMenuItemClick: deleting current Service User Name " +
                        currentServiceUserName);
                deleteCurrentServiceUserName();
                return true;
            }
        });
        menu.findItem(R.id.action_logout).setVisible(true);
        menu.findItem(R.id.action_logout).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                logout();
                return true;
            }
        });
        super.onPrepareOptionsMenu(menu);
    }


    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        View fragmentView = inflater.inflate(R.layout.generator_layout, container, false);
        dataSource = new DataSource(getActivity());
        fragmentManager = getFragmentManager();

        if (this.getArguments() != null) {
            username = this.getArguments().getString("username");
            userId = this.getArguments().getInt("userId");

            serviceNameSpinner = fragmentView.findViewById(R.id.id_generate_service_spinner);
            serviceUserNameSpinner = fragmentView.findViewById(R.id.id_generate_username_spinner);
            serviceVersionSpinner = fragmentView.findViewById(R.id.id_generate_version_spinner);
            passwordOutputEditText = fragmentView.findViewById(R.id.id_generate_generated_password);
            passwordLengthEditText = fragmentView.findViewById(R.id.id_generate_password_length);
            specialCheckBox = fragmentView.findViewById(R.id.id_generate_special_check);
            Button generateButton = fragmentView.findViewById(R.id.id_generate_generate_btn);

            generateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    generateServicePassword();
                }
            });


            Button raiseVersionButton = fragmentView.findViewById(R.id.id_generate_raise_version_btn);
            TextView addNewServiceButton = fragmentView.findViewById(R.id.id_generate_new_service_btn);

            raiseVersionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    raiseVersion();
                }
            });
            addNewServiceButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addService();
                }
            });
            loadSpinners();
        }
        return fragmentView;
    }

    /**
     * As soon as a service is created it takes
     * the userId, serviceName, serviceUserName, latestVersion, passwordLength and passes them to
     * PasswordGeneratorHelper.generateIndividualServiceHash where the password will be created.
     * also it checks whether special characters are being wanted or not.
     */
    private void generateServicePassword() {
        if (serviceNameSpinner.getSelectedItem() == null) {
            return;
        }
        String service = serviceNameSpinner.getSelectedItem().toString();
        String username = serviceUserNameSpinner.getSelectedItem().toString();
        String version = serviceVersionSpinner.getSelectedItem().toString();

        int passwordLength;
        try {
            passwordLength = Integer.parseInt(
                    passwordLengthEditText.getText().toString());
        } catch (java.lang.NumberFormatException e) {
            //            When no length is provided
            passwordLength = 8;
        }

        boolean checkSpecialChar = specialCheckBox.isChecked();

        List<String> components = Arrays.asList(
                String.valueOf(userId),
                service,
                username,
                version,
                String.valueOf(passwordLength));

        String generatedPassword = pGHelper.generateIndividualServiceHash(
                components,
                passwordLength,
                checkSpecialChar);

        passwordOutputEditText.setText(generatedPassword);
    }


    /**
     * Ask the user to delete the current serviceUserName. If serviceUserName is the only user in
     * respective service then delete service as well.
     */
    private void deleteCurrentServiceUserName() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Service User Deletion");
        dialog.setMessage(
                String.format("Do you want to delete \"%s\" from your \"%s\" Service?",
                        currentServiceUserName, currentService));
        dialog.setCancelable(true);
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dataSource.open();
                dataSource.deleteServiceUserName(userId, currentService, currentServiceUserName);
                dataSource.close();
                loadSpinners();
                dialog.dismiss();
                Toast.makeText(
                        getActivity(), "User deleted.", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    /**
     * Retrieves the LinkedHashMap userServices and sets adapters to respective spinners.
     */
    private void loadSpinners() {
        Log.d(TAG, "loadSpinners: starting...");
        generateUserServicesMap(userId);
        setServiceNameAdapter();
        serviceNameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentService = serviceNameArray.get(position);
                Log.d(TAG, "currentService: " + currentService);
                setServiceUserNameAdapter(currentService);
                passwordOutputEditText.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        serviceUserNameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentServiceUserName = serviceUserNameArray.get(position);
                currentVersion = (int) Objects.requireNonNull(
                        userServices.get(currentService)).get(currentServiceUserName);
                Log.d(TAG, "currentServiceUserName: " + currentServiceUserName);
                Log.d(TAG, "currentVersion: " + currentVersion);
                setServiceVersionAdapter(currentVersion);
                passwordOutputEditText.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        Log.d(TAG, "loadSpinners: exiting...");
    }


    /**
     * Creates a list from 1 to whatever is latestVersion of Service
     * and sets up the serviceVersionSpinner.
     *
     * @param currentVersion currently selected Version
     */
    private void setServiceVersionAdapter(int currentVersion) {
        List<Integer> serviceVersionArray = new ArrayList<>();
        for (int i = currentVersion; i >= 1; i--) {
            serviceVersionArray.add(i);
        }
        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(
                Objects.requireNonNull(getActivity()),
                android.R.layout.simple_spinner_item,
                serviceVersionArray
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        serviceVersionSpinner.setAdapter(adapter);
    }


    /**
     * Retrieve all keys from LinkedHashMap userServices and stores them in a list which then
     * will be used by the adapter to set up serviceUserNameSpinner.
     */
    private void setServiceNameAdapter() {
        serviceNameArray = new ArrayList<>(userServices.keySet());
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                Objects.requireNonNull(getActivity()),
                android.R.layout.simple_spinner_item,
                serviceNameArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        serviceNameSpinner.setAdapter(adapter);

    }

    /**
     * Retrieve all serviceUserNames from respective key from LinkedHashMap userServices and store
     * them in a list for the adapter to be used.
     *
     * @param currentService currently selected service
     */
    private void setServiceUserNameAdapter(String currentService) {
        serviceUserNameArray = new ArrayList<>();
        serviceUserNameArray.addAll(
                Objects.requireNonNull(userServices.get(currentService)).keySet());
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                Objects.requireNonNull(getActivity()),
                android.R.layout.simple_spinner_item,
                serviceUserNameArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        serviceUserNameSpinner.setAdapter(adapter);
    }


    /**
     * Retrieves all services know to the user from database and puts them into a LinkedHashMap.
     * example:
     * {
     * "FooService": {
     * "barUser": 1,
     * "cooUser": 5
     * }
     * "BarService": {
     * "dooUser": 3
     * }
     * }
     *
     * @param userId id of user
     */
    private void generateUserServicesMap(int userId) {
        String serviceName;
        String serviceUserName;
        int latestVersion;

        dataSource.open();
        Cursor cursor = dataSource.getServices(userId);
        userServices = new LinkedHashMap<>();
        LinkedHashMap<String, Object> namesOfService;


        if (!(cursor.getCount() > 0)) {
            return;
        }

        while (!cursor.isLast()) {
            cursor.moveToNext();
            serviceName = cursor.getString(0);
            serviceUserName = cursor.getString(1);
            latestVersion = cursor.getInt(2);
            LinkedHashMap<String, Object> namePlusVersion = new LinkedHashMap<>();

            if (!userServices.containsKey(serviceName)) {
                namePlusVersion.put(serviceUserName, latestVersion);
                userServices.put(serviceName, namePlusVersion);
            } else {
                namesOfService = userServices.get(serviceName);
                Objects.requireNonNull(namesOfService).put(serviceUserName, latestVersion);
                userServices.put(serviceName, namesOfService);
            }
        }
        cursor.close();
        dataSource.close();
        Log.d(TAG, "generateUserServicesMap: " + userServices);
    }


    /**
     * Ask the user to create a new serviceName and serviceUserName and persist them in the
     * database.
     */
    private void addService() {
        final Dialog dialog = new Dialog(Objects.requireNonNull(getActivity()));
        dialog.setContentView(R.layout.add_service_layout);
        dialog.setTitle("Create New Service");
        dialog.setCancelable(true);
        dialog.show();

        final EditText serviceNameInput = dialog.findViewById(R.id.id_service_servicename_input);
        final EditText serviceUserNameInput = dialog.findViewById(R.id.id_service_username_input);

        dialog.findViewById(R.id.id_service_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String serviceName = StringUtils.capitalize(serviceNameInput
                        .getText()
                        .toString()
                        .toLowerCase()
                        .trim());
                final String serviceUserName = serviceUserNameInput
                        .getText()
                        .toString()
                        .trim();

                if (serviceName.isEmpty() | serviceUserName.isEmpty()) {
                    Toast.makeText(
                            getActivity(),
                            "Please enter a Service and Username",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                Log.d(TAG, String.format("onClick: saving new sevice\n" +
                                "Service Name: %s\n" +
                                "Service Username: %s\n" +
                                "Username: %s\n" +
                                "UserId %s",
                        serviceName,
                        serviceUserName,
                        username,
                        userId));

                dataSource.open();
                if (!dataSource.saveNewServiceSuccessful(userId, serviceName, serviceUserName)) {
                    Toast.makeText(
                            getActivity(),
                            String.format("Service %s with ServiceUsername %s already exists!",
                                    serviceName, serviceUserName),
                            Toast.LENGTH_SHORT).show();
                    dataSource.close();
                    return;
                }
                dataSource.close();
                dialog.dismiss();
                loadSpinners();
            }
        });

        dialog.findViewById(R.id.id_service_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }


    /**
     * Ask the user to raise the versioning of currently selected serviceUserName which will raise
     * latestVersion by +1.
     */
    private void raiseVersion() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Raise Version");
        dialog.setMessage("Do you really want to raise the Version by 1?");
        dialog.setCancelable(true);
        dialog.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        dialog.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dataSource.open();
                        Log.d(TAG, "raiseVersion: " + userId + currentService + currentServiceUserName);
                        dataSource.raiseVersion(userId, currentService, currentServiceUserName);
                        dataSource.close();
                        dialog.dismiss();
                        loadSpinners();
                    }
                });
        dialog.show();
    }


    /**
     * Redirects user to LoginFragment. Since there is no fragmentTransaction.addToBackStack(null)
     * defined a user is not able to get back without logging back in again.
     */
    private void logout() {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        LoginFragment loginFragment = new LoginFragment();
        fragmentTransaction.replace(R.id.ui_container, loginFragment);
        fragmentTransaction.commit();
    }
}
