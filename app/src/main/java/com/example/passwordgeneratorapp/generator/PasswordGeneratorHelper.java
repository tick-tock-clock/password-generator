package com.example.passwordgeneratorapp.generator;

import android.util.Log;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/** Holds all logic for working with passwords.*/
public class PasswordGeneratorHelper {
    private final String TAG = PasswordGeneratorHelper.class.getSimpleName();

    private static final String UPPER_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String LOWER_LETTERS = "abcdefghijklmnopqrstuvwxyz";
    private static final String NUMBERS = "0123456789";
    private static final String SPECIAL_CHARS = "!#@$%^&*()=/,|.:[;]{}-+";

    private static final int ITERATIONS = 100000;
    private static final int KEY_LENGTH = 256;
    private static final String SALT_ALGORITHM = "SHA1PRNG";
    private final String PBKDF_ALGORITHM = "PBKDF2WithHmacSHA1";


    /**
     * Concatenates all input into one String and stores that hashCode value (Integer) into the
     * seed of a Random object. That way the password is expected to always be same for
     * that specific concatenated String/hashCode.
     * @param components List of user inputs
     * @param passwordLength Integer
     * @param checkSpecialChar boolean whether to use them or not
     * @return The generated password
     */
    String generateIndividualServiceHash(
            List<String> components,
            int passwordLength,
            boolean checkSpecialChar) {

        Log.d(TAG, "generateIndividualServiceHash: Generating Service Password.");
        String allComponents = String.join("", components);
        int seed = allComponents.hashCode();
        Random randomGenerator = new Random(seed);

        if (passwordLength > 16) {
            passwordLength = 16;
        }
        if (passwordLength < 4) {
            passwordLength = 4;
        }

        String specialChars = "";
        if (checkSpecialChar) {
            specialChars = SPECIAL_CHARS;
        }

        char[] allChars = String.join(
                "",
                UPPER_LETTERS,
                LOWER_LETTERS,
                NUMBERS, specialChars
        ).toCharArray();

        StringBuilder hash = new StringBuilder();

        for (int i = 0; i < passwordLength; i++) {
            int rndNumber = randomGenerator.nextInt(allChars.length);
            hash.append(allChars[rndNumber]);
        }
        return String.valueOf(hash);
    }

    /**
     * Hexadecimal value of the generated password hash and salt using PBKDF_ALGORITHM.
     * @param password The user password
     * @return A List of the hash and salt in hexadecimal
     * @throws NoSuchAlgorithmException If PBKDF_ALGORITHM does not exist.
     * @throws InvalidKeySpecException If KeySpecs do not match.
     */
    public List<String> generatePasswordHashForDb(String password)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        Log.d(TAG, "generatePasswordHashForDb: Generating DB Password Hash.");
        char[] chars = password.toCharArray();
        byte[] salt = getSalt();

        PBEKeySpec spec = new PBEKeySpec(chars, salt, ITERATIONS, KEY_LENGTH);
        SecretKeyFactory skf = SecretKeyFactory.getInstance(PBKDF_ALGORITHM);
        byte[] hash = skf.generateSecret(spec).getEncoded();

        return Arrays.asList(toHex(hash), toHex(salt));
    }

    /**
     * Generates a random salt.
     * @return generated salt
     * @throws NoSuchAlgorithmException If SALT_ALGORITHM does not exist.
     */
    private static byte[] getSalt() throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance(SALT_ALGORITHM);
        byte[] salt = new byte[16];
        sr.nextBytes(salt);

        return salt;
    }

    /**
     * Takes the user password and the salt from respective user from database and generate
     * a testHash of it. Then compares the dbHash and testHash bitwise with each other.
     * @param originalPassword from user input.
     * @param queryResults from database.
     * @return true if testHash matches dbHash.
     * @throws NoSuchAlgorithmException If PBKDF_ALGORITHM does not exist.
     * @throws InvalidKeySpecException If KeySpecs do not match.
     */
    public boolean validatePassword(String originalPassword, List<String> queryResults)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        Log.d(TAG, "validatePassword: Validating User Password.");

        byte[] dbHash = fromHex(queryResults.get(0));
        byte[] dbSalt = fromHex(queryResults.get(1));

        PBEKeySpec spec = new PBEKeySpec(
                originalPassword.toCharArray(),
                dbSalt,
                ITERATIONS,
                dbHash.length * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance(PBKDF_ALGORITHM);
        byte[] testHash = skf.generateSecret(spec).getEncoded();

        int diff = dbHash.length ^ testHash.length;
        for (int i = 0; i < dbHash.length && i < testHash.length; i++) {
            diff |= dbHash[i] ^ testHash[i];
        }
        return diff == 0;
    }

    /**
     * Converts into hexadecimal.
     * @param array ByteArray
     * @return Hexadecimal as String.
     */
    private static String toHex(byte[] array) {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) {
            return String.format("%0" + paddingLength + "d", 0) + hex;
        } else {
            return hex;
        }
    }

    /**
     * Turns String into ByteArray.
     * @param hex String
     * @return ByteArray
     */
    private static byte[] fromHex(String hex) {
        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }

}
