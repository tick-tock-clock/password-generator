package com.example.passwordgeneratorapp;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.passwordgeneratorapp.db.handlers.DataSource;
import com.example.passwordgeneratorapp.generator.PasswordGeneratorFragment;

import java.util.Arrays;
import java.util.List;


/** Allows the user to create a new user account. */
public class CreateUserFragment extends Fragment {
    private static final String TAG = PasswordGeneratorFragment.class.getSimpleName();

    private DataSource dataSource;
    private FragmentManager fragmentManager;


    private EditText usernameInput;
    private EditText masterInput1;
    private EditText masterInput2;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.create_user_layout, container, false);
        dataSource = new DataSource(getActivity());
        fragmentManager = getFragmentManager();

        usernameInput = fragmentView.findViewById(R.id.id_create_user_input);
        masterInput1 = fragmentView.findViewById(R.id.id_create_master1_input);
        masterInput2 = fragmentView.findViewById(R.id.id_create_master2_input);

        Button createUserButton = fragmentView.findViewById(R.id.id_create_create_btn);
        createUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createUser();
            }
        });


        return fragmentView;
    }


    /**
     * Persists a new User in database. First makes sure all inputs are filled correctly.
     * Then hands the input as List to DataSource.createNewUser. If persistence successful
     * then redirect to LoginFragment.
     */
    private void createUser() {

        Log.d(TAG, "Pressed Create User");
        String username = usernameInput.getText().toString();
        String master_1 = masterInput1.getText().toString();
        String master_2 = masterInput2.getText().toString();

        if (username.isEmpty() | master_1.isEmpty() | master_2.isEmpty()) {
            Toast.makeText(
                    getActivity(), "Please fill out all fields!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!master_1.equals(master_2)) {
            Toast.makeText(
                    getActivity(), "Your Passwords do not match!", Toast.LENGTH_SHORT).show();
            return;
        }
        List<String> credentials = Arrays.asList(username, master_1);
        dataSource.open();
        if (dataSource.createNewUser(credentials)) {
            Toast.makeText(
                    getActivity(), "User created successfully.", Toast.LENGTH_SHORT).show();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            LoginFragment loginFragment = new LoginFragment();
            fragmentTransaction.replace(R.id.ui_container, loginFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        } else {
            Toast.makeText(
                    getActivity(), "Username already exists.", Toast.LENGTH_SHORT).show();
        }
        dataSource.close();
    }
}
